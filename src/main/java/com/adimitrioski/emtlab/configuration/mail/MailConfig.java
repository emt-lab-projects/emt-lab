package com.adimitrioski.emtlab.configuration.mail;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.Properties;

@Configuration
public class MailConfig {

    private static final String SPRING_MAIL_HOST = "spring.mail.host";
    private static final String SPRING_MAIL_PORT = "spring.mail.port";
    private static final String SPRING_MAIL_PROPERITES_MAIL_SMTPS_STARTTLS_ENABLE = "spring.mail.properties.mail.smtps.starttls.enable";
    private static final String SPRING_MAIL_PROTOCOL = "spring.mail.protocol";
    private static final String SPRING_MAIL_USERNAME = "spring.mail.username";
    private static final String SPRING_MAIL_PASSWORD = "spring.mail.password";
    private static final String SPRING_MAIL_PROPERTIES_MAIL_TRANSPORT_PROTOCOL = "spring.mail.properties.mail.transport.protocol";
    private static final String SPRING_MAIL_PROPERTIES_MAIL_SMTPS_AUTH = "spring.mail.properties.mail.smtps.auth";

    private final Environment environment;

    @Autowired
    public MailConfig(Environment environment) {
        this.environment = environment;
    }

    @Bean
    public JavaMailSender javaMailSender() {

        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();

        String host = environment.getProperty(SPRING_MAIL_HOST);
        Integer port = Integer.parseInt(environment.getProperty(SPRING_MAIL_PORT));
        String username = environment.getProperty(SPRING_MAIL_USERNAME);
        String password = environment.getProperty(SPRING_MAIL_PASSWORD);

        mailSender.setHost(host);
        mailSender.setPort(port);
        mailSender.setUsername(username);
        mailSender.setPassword(password);

        String transportProtocol = environment.getProperty(SPRING_MAIL_PROPERTIES_MAIL_TRANSPORT_PROTOCOL);
        String auth = environment.getProperty(SPRING_MAIL_PROPERTIES_MAIL_SMTPS_AUTH);
        String starttls = environment.getProperty(SPRING_MAIL_PROPERITES_MAIL_SMTPS_STARTTLS_ENABLE);

        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.transport.protocol", transportProtocol);
        props.put("mail.smtp.auth", auth);
        props.put("mail.smtp.starttls.enable", starttls);
        props.put("mail.debug", "true");
        return mailSender;
    }


}
