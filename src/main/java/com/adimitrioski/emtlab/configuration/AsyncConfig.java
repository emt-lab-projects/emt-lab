package com.adimitrioski.emtlab.configuration;

import org.springframework.scheduling.annotation.EnableAsync;

@EnableAsync
public class AsyncConfig {
}
