package com.adimitrioski.emtlab.configuration.security;

import com.adimitrioski.emtlab.model.domain.Department;
import com.adimitrioski.emtlab.model.domain.Employee;
import com.adimitrioski.emtlab.model.enumeration.Role;
import com.adimitrioski.emtlab.model.security.EmployeeUserDetails;
import com.adimitrioski.emtlab.repository.DepartmentRepository;
import com.adimitrioski.emtlab.repository.EmployeeRepository;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.social.github.api.GitHub;
import org.springframework.social.github.api.GitHubUserProfile;
import org.springframework.social.github.api.impl.GitHubTemplate;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Component
public class GithubAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

    private final EmployeeRepository employeeRepository;
    private final DepartmentRepository departmentRepository;

    public GithubAuthenticationSuccessHandler(EmployeeRepository employeeRepository, DepartmentRepository departmentRepository) {
        this.employeeRepository = employeeRepository;
        this.departmentRepository = departmentRepository;
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {

        String token = ((OAuth2AuthenticationDetails) authentication.getDetails()).getTokenValue();

        GitHub github = new GitHubTemplate(token);

        if (github.isAuthorized()) {
            GitHubUserProfile gitHubUserProfile = github.userOperations().getUserProfile();
            String fullName = gitHubUserProfile.getName();
            String[] splitName = fullName.split(" ");
            String firstName = splitName[0];
            String lastName = splitName[1];
            String email = gitHubUserProfile.getEmail();

            Optional<Employee> employeeOptional;
            Employee employee = new Employee();

            employeeOptional = employeeRepository.findByEmail(email);
            if (!employeeOptional.isPresent()) {
                employee.email = email;
                employee.firstName = firstName;
                employee.lastName = lastName;
                employee.active = true;
                employee.registrationDate = LocalDateTime.now();
                employee.role = Role.EMPLOYEE;

                Optional<Department> department = departmentRepository.findById((long) 0);
                if (department.isPresent()) {
                    employee.department = department.get();
                } else throw new DataRetrievalFailureException("Department not found, employee cannot be registered");
                employee = employeeRepository.save(employee);
            } else {
                employee = employeeOptional.get();
            }

            Role employeeRole = employee.role;

            Set<GrantedAuthority> authorities = new HashSet<>();
            authorities.add(new SimpleGrantedAuthority(employeeRole.toString()));

            EmployeeUserDetails employeeUserDetails = new EmployeeUserDetails(employee);
            employeeUserDetails.setEmployee(employee);
            employeeUserDetails.setAuthorities(authorities);

            Authentication newAuthentication =
                    new UsernamePasswordAuthenticationToken(employeeUserDetails, null, employeeUserDetails.getAuthorities());

            SecurityContextHolder.getContext().setAuthentication(newAuthentication);

            //System.out.println("ID: " + id + " First Name: " + firstName + " Last Name: " + lastName + " Email: " + email);
            response.sendRedirect("/");
        }

    }
}
