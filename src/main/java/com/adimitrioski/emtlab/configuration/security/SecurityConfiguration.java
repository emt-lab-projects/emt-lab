package com.adimitrioski.emtlab.configuration.security;

import com.adimitrioski.emtlab.service.implementation.security.EmployeeUserDetailsService;
import com.adimitrioski.emtlab.service.implementation.security.IntermediateUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.oauth2.resource.ResourceServerProperties;
import org.springframework.boot.autoconfigure.security.oauth2.resource.UserInfoTokenServices;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.filter.OAuth2ClientAuthenticationProcessingFilter;
import org.springframework.security.oauth2.client.filter.OAuth2ClientContextFilter;
import org.springframework.security.oauth2.client.token.grant.code.AuthorizationCodeResourceDetails;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.web.filter.CompositeFilter;

import javax.servlet.Filter;
import java.util.ArrayList;
import java.util.List;

@Configuration
@EnableOAuth2Client
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    private final EmployeeUserDetailsService employeeUserDetailsService;
    private final IntermediateUserDetailsService intermediateUserDetailsService;
    private final OAuth2ClientContext oAuth2ClientContext;
    private final FacebookAuthenticationSuccessHandler facebookAuthenticationSuccessHandler;
    private final GithubAuthenticationSuccessHandler githubAuthenticationSuccessHandler;

    @Autowired
    public SecurityConfiguration(EmployeeUserDetailsService employeeUserDetailsService, IntermediateUserDetailsService intermediateUserDetailsService,
                                 OAuth2ClientContext oAuth2ClientContext, FacebookAuthenticationSuccessHandler facebookAuthenticationSuccessHandler,
                                 GithubAuthenticationSuccessHandler githubAuthenticationSuccessHandler) {
        this.employeeUserDetailsService = employeeUserDetailsService;
        this.intermediateUserDetailsService = intermediateUserDetailsService;
        this.oAuth2ClientContext = oAuth2ClientContext;
        this.facebookAuthenticationSuccessHandler = facebookAuthenticationSuccessHandler;
        this.githubAuthenticationSuccessHandler = githubAuthenticationSuccessHandler;
    }

    @Bean
    public FormLoginAuthenticationProvider authenticationProvider() {
        FormLoginAuthenticationProvider formLoginAuthenticationProvider = new FormLoginAuthenticationProvider();
        formLoginAuthenticationProvider.setEmployeeUserDetailsService(employeeUserDetailsService);
        formLoginAuthenticationProvider.setPasswordEncoder(passwordEncoder());
        return formLoginAuthenticationProvider;
    }

    @Bean
    public CustomLogoutHandler logoutHandler() {
        return new CustomLogoutHandler(intermediateUserDetailsService);
    }


    @Override
    protected void configure(
            AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authenticationProvider());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .x509().subjectPrincipalRegex("CN=(.*?)(?:,|$)").userDetailsService(intermediateUserDetailsService)
                .and().addFilter(new X509ErrorCheckerFilter())
                .formLogin().loginPage("/employees/login")
                .and()
                .rememberMe().key("uniqueRememberMeKey").tokenValiditySeconds(2592000)
                .and()
                .logout().addLogoutHandler(logoutHandler()).deleteCookies("remember-me")
                .and()
                .authorizeRequests().antMatchers("/employees/edit/my-profile", "/employees/change-password").access("hasAnyAuthority('ADMIN', 'MANAGER', 'EMPLOYEE')")
                .and()
                .authorizeRequests().antMatchers("/employees/manage", "/employees/edit/*", "/employees/delete/*", "/employees/new").access("hasAnyAuthority('ADMIN', 'MANAGER')")
                .and()
                .authorizeRequests().antMatchers("/departments/manage", "/departments/edit/*", "/departments/new").access("hasAnyAuthority('ADMIN')")
                .and()
                .authorizeRequests().antMatchers("/", "/employees/login", "/employees/activate", "/employees/register", "employees/request-password-reset").access("hasAnyAuthority('ADMIN', 'MANAGER', 'EMPLOYEE', 'INTERMEDIATE')")
                .and()
                .authorizeRequests().antMatchers("/error").permitAll()
                .and().addFilterBefore(ssoFilter(), BasicAuthenticationFilter.class);
    }


    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    private Filter ssoFilter() {
        CompositeFilter filter = new CompositeFilter();
        List<Filter> filters = new ArrayList<>();
        filters.add(ssoFilter(facebook(), "/login/facebook"));
        filters.add(ssoFilter(github(), "/login/github"));
        filter.setFilters(filters);
        return filter;
    }

    private Filter ssoFilter(ClientResources client, String path) {
        OAuth2ClientAuthenticationProcessingFilter filter = new OAuth2ClientAuthenticationProcessingFilter(path);
        OAuth2RestTemplate template = new OAuth2RestTemplate(client.getClient(), oAuth2ClientContext);
        filter.setRestTemplate(template);
        UserInfoTokenServices tokenServices = new UserInfoTokenServices(
                client.getResource().getUserInfoUri(), client.getClient().getClientId());
        tokenServices.setRestTemplate(template);
        filter.setTokenServices(tokenServices);
        if (path.compareTo("/login/facebook") == 0) {
            filter.setAuthenticationSuccessHandler(facebookAuthenticationSuccessHandler);
        }
        if (path.compareTo("/login/github") == 0) {
            filter.setAuthenticationSuccessHandler(githubAuthenticationSuccessHandler);
        }
        return filter;
    }

    @Bean
    public FilterRegistrationBean oauth2ClientFilterRegistration(
            OAuth2ClientContextFilter filter) {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(filter);
        registration.setOrder(-100);
        return registration;
    }

    @Bean
    @ConfigurationProperties("github")
    public ClientResources github() {
        return new ClientResources();
    }

    @Bean
    @ConfigurationProperties("facebook")
    public ClientResources facebook() {
        return new ClientResources();
    }

    class ClientResources {

        @NestedConfigurationProperty
        private AuthorizationCodeResourceDetails client = new AuthorizationCodeResourceDetails();

        @NestedConfigurationProperty
        private ResourceServerProperties resource = new ResourceServerProperties();

        public AuthorizationCodeResourceDetails getClient() {
            return client;
        }

        public ResourceServerProperties getResource() {
            return resource;
        }
    }


}

