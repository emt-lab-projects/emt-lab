package com.adimitrioski.emtlab.configuration.security;

import com.adimitrioski.emtlab.model.security.EmployeeUserDetails;
import com.adimitrioski.emtlab.service.implementation.security.EmployeeUserDetailsService;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class FormLoginAuthenticationProvider implements AuthenticationProvider {

    private EmployeeUserDetailsService employeeUserDetailsService;
    private PasswordEncoder passwordEncoder;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

        String email = authentication.getName();
        String password = authentication.getCredentials().toString();

        EmployeeUserDetails employeeUserDetails = (EmployeeUserDetails) employeeUserDetailsService.loadUserByUsername(email);

        if (!passwordEncoder.matches(password, employeeUserDetails.getPassword())) {
            throw new BadCredentialsException("Password incorrect, login failed.");
        }

        return new UsernamePasswordAuthenticationToken(employeeUserDetails, password, employeeUserDetails.getAuthorities());
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }

    public void setEmployeeUserDetailsService(EmployeeUserDetailsService employeeUserDetailsService) {
        this.employeeUserDetailsService = employeeUserDetailsService;
    }

    public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

}
