package com.adimitrioski.emtlab.configuration.security;

import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;
import org.springframework.security.web.authentication.preauth.x509.SubjectDnX509PrincipalExtractor;
import org.springframework.security.web.authentication.preauth.x509.X509PrincipalExtractor;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.cert.X509Certificate;

public class X509ErrorCheckerFilter extends AbstractPreAuthenticatedProcessingFilter {

    private X509PrincipalExtractor principalExtractor = new SubjectDnX509PrincipalExtractor();

    @Override
    protected Object getPreAuthenticatedPrincipal(HttpServletRequest request) {
        X509Certificate cert = extractClientCertificate(request);
        if (cert == null) {
            return null;
        }
        return principalExtractor.extractPrincipal(cert);
    }

    @Override
    protected Object getPreAuthenticatedCredentials(HttpServletRequest request) {
        return extractClientCertificate(request);
    }

    private X509Certificate extractClientCertificate(HttpServletRequest request) {
        X509Certificate[] certs = (X509Certificate[]) request.getAttribute("javax.servlet.request.X509Certificate");
        if (certs != null && certs.length > 0) {
            return certs[0];
        }
        return null;
    }

    public void setPrincipalExtractor(X509PrincipalExtractor principalExtractor) {
        this.principalExtractor = principalExtractor;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        if (!((HttpServletRequest) request).getRequestURL().toString().endsWith("/error")) {
            X509Certificate extractedCertificate = extractClientCertificate((HttpServletRequest) request);
            if (extractedCertificate == null) {
                logger.info("Detected null certificate");
                HttpServletResponse httpResponse = (HttpServletResponse) response;
                httpResponse.sendRedirect("/error");
                return;
            } else logger.info("Filter executed successfully!");
            chain.doFilter(request, response);
        } else
            chain.doFilter(request, response);
    }

}
