package com.adimitrioski.emtlab.configuration.security;

import com.adimitrioski.emtlab.model.domain.Employee;
import com.adimitrioski.emtlab.model.security.EmployeeUserDetails;
import com.adimitrioski.emtlab.model.security.IntermediateUserDetails;
import com.adimitrioski.emtlab.service.implementation.security.IntermediateUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Component
public class CustomLogoutHandler implements LogoutHandler {

    private final IntermediateUserDetailsService intermediateUserDetailsService;

    @Autowired
    public CustomLogoutHandler(IntermediateUserDetailsService intermediateUserDetailsService) {
        this.intermediateUserDetailsService = intermediateUserDetailsService;
    }

    @Override
    public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {

        HttpSession httpSession = request.getSession(false);
        if (httpSession != null) {
            httpSession.invalidate();
        }

        EmployeeUserDetails employeeUserDetails = (EmployeeUserDetails) authentication.getPrincipal();
        Employee employee = employeeUserDetails.getEmployee();

        IntermediateUserDetails intermediateUserDetails = (IntermediateUserDetails) intermediateUserDetailsService.loadUserByUsername(employee.email);

        Authentication newAuthentication = new UsernamePasswordAuthenticationToken(intermediateUserDetails, null, intermediateUserDetails.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(newAuthentication);
    }

}
