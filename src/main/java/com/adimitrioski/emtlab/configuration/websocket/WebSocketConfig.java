package com.adimitrioski.emtlab.configuration.websocket;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.SockJsServiceRegistration;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {

    public static final String TOPIC_PREFIX = "/room";
    public static final String DEFAULT_TOPIC = "/emt";
    public static final String BROKER_PREFIX = "/broker";
    public static final String SOCKET_URL = "/chat";


    @Override
    public void configureMessageBroker(MessageBrokerRegistry messageBrokerRegistry) {
        messageBrokerRegistry.enableSimpleBroker(TOPIC_PREFIX);
        messageBrokerRegistry.setApplicationDestinationPrefixes(BROKER_PREFIX);
    }

    @Override
    public void registerStompEndpoints(StompEndpointRegistry stompEndpointRegistry) {
        SockJsServiceRegistration reg = stompEndpointRegistry
                .addEndpoint(SOCKET_URL)
                .withSockJS();
    }

}
