package com.adimitrioski.emtlab.service;

import com.adimitrioski.emtlab.model.domain.Department;
import com.adimitrioski.emtlab.model.dto.DepartmentDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface DepartmentService {

    Department createNewDepartment(DepartmentDto departmentDto);

    Department save(Department department);

    Department updateDepartment(Long id, DepartmentDto departmentDto);

    Department delete(Long id);

    Department findById(Long id);

    Department findByName(String name);

    List<Department> findAll();

    Page<Department> findAll(Pageable pageable);

    Long count();

}
