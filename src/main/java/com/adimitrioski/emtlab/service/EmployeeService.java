package com.adimitrioski.emtlab.service;

import com.adimitrioski.emtlab.model.domain.Employee;
import com.adimitrioski.emtlab.model.dto.EmployeeDto;
import com.adimitrioski.emtlab.model.enumeration.Gender;
import com.adimitrioski.emtlab.model.enumeration.Role;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.io.IOException;
import java.util.List;

public interface EmployeeService {

    Employee registerNewEmployee(EmployeeDto employeeDto) throws IOException;

    Employee addNewEmployee(EmployeeDto employeeDto) throws IOException;

    Employee save(Employee employee);

    Employee updateEmployee(Long id, EmployeeDto employeeDto) throws IOException;

    Employee activateEmployee(String activationCode);

    Employee requestResetPasswordEmployee(String email);

    Employee changePassword(Employee employee, String oldPassword, String newPassword);

    Employee delete(Long id);

    Employee findById(Long id);

    Employee findByEmail(String email);

    Page<Employee> findAll(Pageable pageable);

    List<Employee> findAll();

    List<Employee> findByDepartment(Long departmentId);

    List<Employee> findByRole(Role role);

    List<Employee> findByGender(Gender gender);

    List<Employee> findByActive(Boolean active);

    Long count();

}
