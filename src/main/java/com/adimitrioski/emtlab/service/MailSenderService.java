package com.adimitrioski.emtlab.service;

import com.adimitrioski.emtlab.model.domain.Employee;

public interface MailSenderService {

    void sendEmail(Employee receiver, String subject, String content, boolean isHtml);

}
