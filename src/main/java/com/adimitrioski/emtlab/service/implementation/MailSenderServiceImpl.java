package com.adimitrioski.emtlab.service.implementation;

import com.adimitrioski.emtlab.model.domain.Employee;
import com.adimitrioski.emtlab.service.MailSenderService;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class MailSenderServiceImpl implements MailSenderService {

    private final ApplicationEventPublisher applicationEventPublisher;
    private final JavaMailSender javaMailSender;

    public MailSenderServiceImpl(ApplicationEventPublisher applicationEventPublisher, JavaMailSender javaMailSender) {
        this.applicationEventPublisher = applicationEventPublisher;
        this.javaMailSender = javaMailSender;
    }

    @Override
    public void sendEmail(Employee receiver, String subject, String content, boolean isHtml) {

        final String to = receiver.email;
        final String from = "emt.lab.test@gmail.com";

        SimpleMailMessage email = new SimpleMailMessage();
        email.setFrom(from);
        email.setTo(to);
        email.setSubject(subject);
        email.setText(content);
        javaMailSender.send(email);
    }
}
