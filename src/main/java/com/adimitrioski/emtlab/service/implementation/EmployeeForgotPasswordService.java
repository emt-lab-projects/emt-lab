package com.adimitrioski.emtlab.service.implementation;

import com.adimitrioski.emtlab.event.EmployeeForgotPasswordEvent;
import com.adimitrioski.emtlab.model.domain.Employee;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import java.util.Locale;

@Service
public class EmployeeForgotPasswordService {

    private final AsyncMailSenderService asyncMailSender;
    private final SpringTemplateEngine springTemplateEngine;

    public EmployeeForgotPasswordService(AsyncMailSenderService asyncMailSender, SpringTemplateEngine springTemplateEngine) {
        this.asyncMailSender = asyncMailSender;
        this.springTemplateEngine = springTemplateEngine;
    }

    public void sendForgotPasswordMail(Employee employee) {

        Locale locale = Locale.getDefault();
        Context context = new Context(locale);
        String content = "New password: " + employee.password;
        asyncMailSender.sendEmail(employee, "Request for resetting password", content, false);

    }

    @EventListener(EmployeeForgotPasswordEvent.class)
    public void onUserRegistration(EmployeeForgotPasswordEvent event) {
        sendForgotPasswordMail(event.getEmployee());
    }

}
