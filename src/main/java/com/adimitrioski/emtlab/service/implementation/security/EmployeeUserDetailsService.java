package com.adimitrioski.emtlab.service.implementation.security;

import com.adimitrioski.emtlab.model.domain.Employee;
import com.adimitrioski.emtlab.model.enumeration.Role;
import com.adimitrioski.emtlab.model.security.EmployeeUserDetails;
import com.adimitrioski.emtlab.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class EmployeeUserDetailsService implements UserDetailsService {

    private final EmployeeRepository employeeRepository;

    @Autowired
    public EmployeeUserDetailsService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<Employee> employeeOptional = employeeRepository.findByEmail(username);

        if (!employeeOptional.isPresent()) throw new UsernameNotFoundException("User employee not found.");

        Employee employee = employeeOptional.get();
        Role employeeRole = employee.role;

        Set<GrantedAuthority> authorities = new HashSet<>();
        authorities.add(new SimpleGrantedAuthority(employeeRole.toString()));

        EmployeeUserDetails employeeUserDetails = new EmployeeUserDetails(employee);
        employeeUserDetails.setAuthorities(authorities);

        return employeeUserDetails;
    }
}
