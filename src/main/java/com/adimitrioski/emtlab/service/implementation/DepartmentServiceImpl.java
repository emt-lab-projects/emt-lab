package com.adimitrioski.emtlab.service.implementation;

import com.adimitrioski.emtlab.model.domain.Department;
import com.adimitrioski.emtlab.model.dto.DepartmentDto;
import com.adimitrioski.emtlab.repository.DepartmentRepository;
import com.adimitrioski.emtlab.service.DepartmentService;
import com.adimitrioski.emtlab.web.exception.DepartmentDuplicateNameException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DepartmentServiceImpl implements DepartmentService {

    private final DepartmentRepository departmentRepository;

    @Autowired
    public DepartmentServiceImpl(DepartmentRepository departmentRepository) {
        this.departmentRepository = departmentRepository;
    }


    @Override
    public Department createNewDepartment(DepartmentDto departmentDto) throws DepartmentDuplicateNameException, DataIntegrityViolationException {

        if (checkForDuplicateName(departmentDto.name))
            throw new DepartmentDuplicateNameException("Duplicate name found!");

        Department department = new Department();
        department.name = departmentDto.name;

        return save(department);

    }

    @Override
    public Department save(Department department) throws DataIntegrityViolationException {
        Department _department = departmentRepository.save(department);
        if (_department == null)
            throw new DataIntegrityViolationException("Department integrity violation, cannot be saved!");
        return _department;
    }

    @Override
    public Department updateDepartment(Long id, DepartmentDto departmentDto) throws DepartmentDuplicateNameException, DataIntegrityViolationException {
        Department department = findById(id);
        if (checkForDuplicateName(departmentDto.name))
            throw new DepartmentDuplicateNameException("Duplicate name found!");
        department.name = departmentDto.name;
        return save(department);
    }


    @Override
    public Department delete(Long id) throws DataRetrievalFailureException {
        Department department = findById(id);
        return departmentRepository.delete(department);
    }

    @Override
    public Department findById(Long id) throws DataRetrievalFailureException {

        Optional<Department> department = departmentRepository.findById(id);

        if (department.isPresent()) {
            return department.get();
        } else throw new DataRetrievalFailureException("Department by id not found!");
    }

    @Override
    public Department findByName(String name) throws DataRetrievalFailureException {
        Optional<Department> department = departmentRepository.findByName(name);

        if (department.isPresent()) {
            return department.get();
        } else throw new DataRetrievalFailureException("Department by name not found!");

    }

    @Override
    public List<Department> findAll() {
        return departmentRepository.findAll();
    }

    @Override
    public Page<Department> findAll(Pageable pageable) {
        return departmentRepository.findAll(pageable);
    }

    @Override
    public Long count() {
        return departmentRepository.count();
    }

    private Boolean checkForDuplicateName(String name) {

        Optional<Department> department = departmentRepository.findByName(name);

        if (department.isPresent()) {
            return true;
        }

        return false;

    }
}
