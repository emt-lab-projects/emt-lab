package com.adimitrioski.emtlab.service.implementation;

import com.adimitrioski.emtlab.configuration.websocket.WebSocketConfig;
import com.adimitrioski.emtlab.service.WebSocketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriTemplate;

@Service
public class WebSocketServiceImpl implements WebSocketService {

    private final SimpMessagingTemplate simpMessagingTemplate;

    @Autowired
    public WebSocketServiceImpl(SimpMessagingTemplate simpMessagingTemplate) {
        this.simpMessagingTemplate = simpMessagingTemplate;
    }

    @Override
    public void send(String topic, Object data, String... topicParams) {
        if (!topic.startsWith(WebSocketConfig.TOPIC_PREFIX)) {
            topic = WebSocketConfig.TOPIC_PREFIX + topic;
        }
        String uri = new UriTemplate(topic).expand((Object[]) topicParams).toString();
        simpMessagingTemplate.convertAndSend(uri, data);

    }

    @Override
    public void sendToUser(String user, String topic, Object data, String... topicParams) {
        if (!topic.startsWith(WebSocketConfig.TOPIC_PREFIX)) {
            topic = WebSocketConfig.TOPIC_PREFIX + topic;
        }
        String uri = new UriTemplate(topic).expand((Object[]) topicParams).toString();
        simpMessagingTemplate.convertAndSendToUser(user, uri, data);

    }
}
