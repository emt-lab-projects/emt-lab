package com.adimitrioski.emtlab.service.implementation;

import com.adimitrioski.emtlab.event.EmployeeAddedEvent;
import com.adimitrioski.emtlab.event.EmployeeRegisteredEvent;
import com.adimitrioski.emtlab.model.domain.Employee;
import com.adimitrioski.emtlab.model.dto.EmployeeAddedEventDto;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import java.util.Locale;

@Service
public class EmployeeMailSenderService {

    private final AsyncMailSenderService asyncMailSender;
    private final SpringTemplateEngine springTemplateEngine;

    public EmployeeMailSenderService(AsyncMailSenderService asyncMailSender, SpringTemplateEngine springTemplateEngine) {
        this.asyncMailSender = asyncMailSender;
        this.springTemplateEngine = springTemplateEngine;
    }

    public void sendEmployeeRegistrationMail(Employee employee) {
        Locale locale = Locale.getDefault();
        Context context = new Context(locale);
        String content = "Activation code: " + employee.activationCode;
        asyncMailSender.sendEmail(employee, "Registration completed", content, false);
    }

    public void sendEmployeeAddedMail(EmployeeAddedEventDto employeeAddedEventDto) {
        Locale locale = Locale.getDefault();
        Context context = new Context(locale);
        Employee employee = employeeAddedEventDto.employee;
        String content = String.format("Hello %s\nYour login password is: %s\nYour activation code is: %s", employee.firstName, employeeAddedEventDto.rawPassword, employee.activationCode);
        asyncMailSender.sendEmail(employee, "Welcome to EmpRegistry", content, false);
    }

    @EventListener(EmployeeRegisteredEvent.class)
    public void onEmployeeRegistrationEvent(EmployeeRegisteredEvent event) {
        sendEmployeeRegistrationMail(event.getEmployee());
    }

    @EventListener(EmployeeAddedEvent.class)
    public void onEmployeeAddedEvent(EmployeeAddedEvent event) {
        sendEmployeeAddedMail(event.getEmployeeAddedEventDto());
    }

}
