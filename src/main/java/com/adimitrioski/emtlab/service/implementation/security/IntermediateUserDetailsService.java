package com.adimitrioski.emtlab.service.implementation.security;

import com.adimitrioski.emtlab.model.domain.Employee;
import com.adimitrioski.emtlab.model.security.IntermediateUser;
import com.adimitrioski.emtlab.model.security.IntermediateUserDetails;
import com.adimitrioski.emtlab.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class IntermediateUserDetailsService implements UserDetailsService {

    private final EmployeeRepository employeeRepository;

    @Autowired
    public IntermediateUserDetailsService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<Employee> employeeOptional = employeeRepository.findByEmail(username);

        if (!employeeOptional.isPresent()) throw new UsernameNotFoundException("User employee not found.");

        Employee employee = employeeOptional.get();
        IntermediateUser intermediateUser = new IntermediateUser();
        intermediateUser.email = employee.email;

        String role = "INTERMEDIATE";

        Set<GrantedAuthority> authorities = new HashSet<>();
        authorities.add(new SimpleGrantedAuthority(role));

        IntermediateUserDetails intermediateUserDetails = new IntermediateUserDetails(intermediateUser);
        intermediateUserDetails.setAuthorities(authorities);

        return intermediateUserDetails;
    }

}
