package com.adimitrioski.emtlab.service.implementation;

import com.adimitrioski.emtlab.model.domain.Employee;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class AsyncMailSenderService extends MailSenderServiceImpl {

    public AsyncMailSenderService(ApplicationEventPublisher applicationEventPublisher, JavaMailSender javaMailSender) {
        super(applicationEventPublisher, javaMailSender);
    }

    @Async
    @Override
    public void sendEmail(Employee receiver, String subject, String content, boolean isHtml) {
        super.sendEmail(receiver, subject, content, isHtml);
    }


}
