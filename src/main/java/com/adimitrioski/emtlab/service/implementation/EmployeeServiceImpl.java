package com.adimitrioski.emtlab.service.implementation;

import com.adimitrioski.emtlab.event.EmployeeAddedEvent;
import com.adimitrioski.emtlab.event.EmployeeForgotPasswordEvent;
import com.adimitrioski.emtlab.event.EmployeeRegisteredEvent;
import com.adimitrioski.emtlab.model.domain.Department;
import com.adimitrioski.emtlab.model.domain.Employee;
import com.adimitrioski.emtlab.model.dto.EmployeeAddedEventDto;
import com.adimitrioski.emtlab.model.dto.EmployeeDto;
import com.adimitrioski.emtlab.model.enumeration.Gender;
import com.adimitrioski.emtlab.model.enumeration.Role;
import com.adimitrioski.emtlab.repository.DepartmentRepository;
import com.adimitrioski.emtlab.repository.EmployeeRepository;
import com.adimitrioski.emtlab.service.EmployeeService;
import com.adimitrioski.emtlab.web.exception.EmployeeAlreadyActivatedException;
import com.adimitrioski.emtlab.web.exception.EmployeeDuplicateEmailException;
import com.adimitrioski.emtlab.web.exception.EmployeeWrongOldPasswordException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeRepository employeeRepository;
    private final DepartmentRepository departmentRepository;
    private final PasswordEncoder passwordEncoder;
    private final ApplicationEventPublisher applicationEventPublisher;

    @Autowired
    public EmployeeServiceImpl(EmployeeRepository employeeRepository, DepartmentRepository departmentRepository, PasswordEncoder passwordEncoder, ApplicationEventPublisher applicationEventPublisher) {
        this.employeeRepository = employeeRepository;
        this.departmentRepository = departmentRepository;
        this.passwordEncoder = passwordEncoder;
        this.applicationEventPublisher = applicationEventPublisher;
    }


    @Override
    public Employee registerNewEmployee(EmployeeDto employeeDto) throws EmployeeDuplicateEmailException, DataRetrievalFailureException, IOException {

        if (checkForDuplicateEmail(employeeDto.email))
            throw new EmployeeDuplicateEmailException("Duplicate email found!");

        Employee employee = new Employee();
        String encodedPassword = passwordEncoder.encode(employeeDto.password);

        employee.email = employeeDto.email;
        employee.password = encodedPassword;
        employee.firstName = employeeDto.firstName;
        employee.lastName = employeeDto.lastName;
        employee.gender = employeeDto.gender;

        Optional<Department> department = departmentRepository.findById(employeeDto.departmentId);
        if (department.isPresent()) {
            employee.department = department.get();
        } else throw new DataRetrievalFailureException("Department not found, employee cannot be registered");

        employee.birthDate = employeeDto.birthDate;

        employee.activationCode = UUID.randomUUID().toString();
        employee.active = false;
        employee.registrationDate = LocalDateTime.now();
        employee.role = Role.EMPLOYEE;
        if (employeeDto.getAvatar() != null)
        employee.avatar = employeeDto.avatar.getBytes();

        applicationEventPublisher.publishEvent(new EmployeeRegisteredEvent(employee));

        return save(employee);
    }


    @Override
    public Employee addNewEmployee(EmployeeDto employeeDto) throws EmployeeDuplicateEmailException, DataRetrievalFailureException, IOException {

        if (checkForDuplicateEmail(employeeDto.email))
            throw new EmployeeDuplicateEmailException("Duplicate email found!");

        Employee employee = new Employee();
        String randomPassword = UUID.randomUUID().toString();
        String randomPasswordEncoded = passwordEncoder.encode(randomPassword);

        employee.email = employeeDto.email;
        employee.password = randomPasswordEncoded;
        employee.firstName = employeeDto.firstName;
        employee.lastName = employeeDto.lastName;
        employee.gender = employeeDto.gender;

        Optional<Department> department = departmentRepository.findById(employeeDto.departmentId);
        if (department.isPresent()) {
            employee.department = department.get();
        } else throw new DataRetrievalFailureException("Department not found, employee cannot be added");

        employee.birthDate = employeeDto.birthDate;

        employee.activationCode = UUID.randomUUID().toString();
        employee.active = false;
        employee.registrationDate = LocalDateTime.now();
        employee.role = employeeDto.role;

        employee.avatar = employeeDto.avatar.getBytes();

        EmployeeAddedEventDto employeeAddedEventDto = new EmployeeAddedEventDto();
        employeeAddedEventDto.employee = employee;
        employeeAddedEventDto.rawPassword = randomPassword;
        applicationEventPublisher.publishEvent(new EmployeeAddedEvent(employeeAddedEventDto));

        return save(employee);
    }

    @Override
    public Employee save(Employee employee) throws DataIntegrityViolationException {
        Employee _employee = employeeRepository.save(employee);
        if (_employee == null)
            throw new DataIntegrityViolationException("Employee integrity violation, cannot be saved!");
        return _employee;
    }

    @Override
    public Employee updateEmployee(Long id, EmployeeDto employeeDto) throws DataRetrievalFailureException, DataIntegrityViolationException, IOException {
        Employee employee = findById(id);

        if (employeeDto.firstName != null) {
            employee.firstName = employeeDto.firstName;
        }
        if (employeeDto.lastName != null) {
            employee.lastName = employeeDto.lastName;
        }
        if (employeeDto.gender != null) {
            employee.gender = employeeDto.gender;
        }
        if (employeeDto.departmentId != null) {
            Optional<Department> department = departmentRepository.findById(employeeDto.departmentId);
            if (department.isPresent()) {
                employee.department = department.get();
            } else throw new DataRetrievalFailureException("Department not found, employee cannot be registered");
        }
        if (employeeDto.birthDate != null) {
            employee.birthDate = employeeDto.birthDate;
        }
        if (employeeDto.role != null) {
            employee.role = employeeDto.role;
        }
        if (employeeDto.avatar != null) {
            employee.avatar = employeeDto.avatar.getBytes();
        }
        return save(employee);
    }

    @Override
    public Employee activateEmployee(String activationCode) throws EmployeeAlreadyActivatedException, DataRetrievalFailureException, DataIntegrityViolationException {

        Optional<Employee> employee = employeeRepository.findByActivationCode(activationCode);

        if (employee.isPresent()) {
            Employee _employee = employee.get();

            if (_employee.active == true)
                throw new EmployeeAlreadyActivatedException("Employee is already activated!");

            _employee.active = true;
            return save(_employee);
        } else throw new DataRetrievalFailureException("Invalid activation code!");

    }

    @Override
    public Employee requestResetPasswordEmployee(String email) throws DataRetrievalFailureException, DataIntegrityViolationException {

        Optional<Employee> employee = employeeRepository.findByEmail(email);
        if (employee.isPresent()) {

            Employee _employee = employee.get();
            String randomPassword = UUID.randomUUID().toString();
            _employee.password = randomPassword;

            applicationEventPublisher.publishEvent(new EmployeeForgotPasswordEvent(_employee));

            String encodedPassword = passwordEncoder.encode(randomPassword);
            _employee.password = encodedPassword;

            return save(_employee);

        } else throw new DataRetrievalFailureException("Invalid email!");

    }

    @Override
    public Employee changePassword(Employee employee, String oldPassword, String newPassword) throws DataIntegrityViolationException, EmployeeWrongOldPasswordException {

        if (!passwordEncoder.matches(oldPassword, employee.password)) {
            throw new EmployeeWrongOldPasswordException("Invalid old password, change password failed.");
        }
        String encodedPassword = passwordEncoder.encode(newPassword);
        employee.password = encodedPassword;
        return save(employee);
    }


    @Override
    public Employee delete(Long id) throws DataRetrievalFailureException {
        Employee employee = findById(id);
        return employeeRepository.delete(employee);
    }

    @Override
    public Employee findById(Long id) throws DataRetrievalFailureException {
        Optional<Employee> employee = employeeRepository.findById(id);

        if (employee.isPresent()) {
            return employee.get();
        } else throw new DataRetrievalFailureException("Employee by id not found!");

    }

    @Override
    public Employee findByEmail(String email) throws DataRetrievalFailureException {
        Optional<Employee> employee = employeeRepository.findByEmail(email);

        if (employee.isPresent()) {
            return employee.get();
        } else throw new DataRetrievalFailureException("Employee by email not found!");

    }

    @Override
    public Page<Employee> findAll(Pageable pageable) {
        return employeeRepository.findAll(pageable);
    }

    @Override
    public List<Employee> findAll() {
        return employeeRepository.findAll();
    }

    @Override
    public List<Employee> findByDepartment(Long departmentId) {
        return employeeRepository.findByDepartment(departmentId);
    }

    @Override
    public List<Employee> findByRole(Role role) {
        return employeeRepository.findByRole(role);
    }

    @Override
    public List<Employee> findByGender(Gender gender) {
        return employeeRepository.findByGender(gender);
    }

    @Override
    public List<Employee> findByActive(Boolean active) {
        return employeeRepository.findByActive(active);
    }

    @Override
    public Long count() {
        return employeeRepository.count();
    }

    private Boolean checkForDuplicateEmail(String email) {
        Optional<Employee> employee = employeeRepository.findByEmail(email);
        if (employee.isPresent()) {
            return true;
        }
        return false;
    }
}
