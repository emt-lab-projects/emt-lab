package com.adimitrioski.emtlab.service.implementation;

import com.adimitrioski.emtlab.model.domain.Department;
import com.adimitrioski.emtlab.model.domain.Employee;
import com.adimitrioski.emtlab.repository.SearchRepository;
import com.adimitrioski.emtlab.service.SearchService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SearchServiceImpl implements SearchService {

    private final SearchRepository searchRepository;

    public SearchServiceImpl(SearchRepository searchRepository) {
        this.searchRepository = searchRepository;
    }

    @Override
    public List<Employee> searchEmployees(String phrase) {
        return searchRepository.searchPhrase(Employee.class, phrase, "email", "firstName", "lastName");
    }

    @Override
    public List<Department> searchDepartments(String phrase) {
        return searchRepository.searchPhrase(Department.class, phrase, "name");
    }
}
