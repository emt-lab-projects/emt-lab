package com.adimitrioski.emtlab.service.implementation;

import com.adimitrioski.emtlab.model.domain.Employee;
import com.adimitrioski.emtlab.repository.EmployeeRepository;
import com.adimitrioski.emtlab.service.EmployeeSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class EmployeeSearchServiceImpl implements EmployeeSearchService {

    private final EmployeeRepository employeeRepository;

    @Autowired
    public EmployeeSearchServiceImpl(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @Override
    public Page<Employee> findByEmail(String email, Pageable pageable) {

        Employee employee = new Employee();
        employee.setEmail(email);
        ExampleMatcher exampleMatcher = createExampleMatcher();
        Example<Employee> employeeExample = Example.of(employee, exampleMatcher);

        return employeeRepository.findAll(employeeExample, pageable);
    }

    @Override
    public Page<Employee> findByFirstName(String firstName, Pageable pageable) {

        Employee employee = new Employee();
        employee.setFirstName(firstName);
        ExampleMatcher exampleMatcher = createExampleMatcher();
        Example<Employee> employeeExample = Example.of(employee, exampleMatcher);

        return employeeRepository.findAll(employeeExample, pageable);

    }

    @Override
    public Page<Employee> findByLastName(String lastName, Pageable pageable) {

        Employee employee = new Employee();
        employee.setLastName(lastName);
        ExampleMatcher exampleMatcher = createExampleMatcher();
        Example<Employee> employeeExample = Example.of(employee, exampleMatcher);

        return employeeRepository.findAll(employeeExample, pageable);

    }

    private ExampleMatcher createExampleMatcher() {
        ExampleMatcher exampleMatcher = ExampleMatcher
                .matching()
                .withIgnoreCase()
                .withIgnoreNullValues()
                .withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING);

        return exampleMatcher;
    }
}
