package com.adimitrioski.emtlab.service;

import com.adimitrioski.emtlab.model.domain.Employee;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface EmployeeSearchService {

    Page<Employee> findByEmail(String email, Pageable pageable);

    Page<Employee> findByFirstName(String firstName, Pageable pageable);

    Page<Employee> findByLastName(String lastName, Pageable pageable);

}
