package com.adimitrioski.emtlab.service;

public interface WebSocketService {

    public void send(String topic, Object data, String... topicParams);

    public void sendToUser(String user, String topic, Object data, String... topicParams);

}
