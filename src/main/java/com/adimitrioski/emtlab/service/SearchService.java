package com.adimitrioski.emtlab.service;

import com.adimitrioski.emtlab.model.domain.Department;
import com.adimitrioski.emtlab.model.domain.Employee;

import java.util.List;

public interface SearchService {
    List<Employee> searchEmployees(String phrase);

    List<Department> searchDepartments(String phrase);
}

