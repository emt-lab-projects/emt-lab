package com.adimitrioski.emtlab.model.websocket;

import java.time.LocalDateTime;

public class ChatMessage {

    public String user;
    public String message;
    public LocalDateTime time;

    public ChatMessage(String user, String message, LocalDateTime time) {
        this.user = user;
        this.message = message;
        this.time = time;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }
}
