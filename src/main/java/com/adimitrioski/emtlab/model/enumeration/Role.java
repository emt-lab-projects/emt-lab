package com.adimitrioski.emtlab.model.enumeration;

public enum Role {
    EMPLOYEE, MANAGER, ADMIN
}
