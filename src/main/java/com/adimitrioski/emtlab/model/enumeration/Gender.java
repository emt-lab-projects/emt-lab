package com.adimitrioski.emtlab.model.enumeration;

public enum Gender {
    MALE, FEMALE
}
