package com.adimitrioski.emtlab.model.dto;

public class DepartmentDto {

    public String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
