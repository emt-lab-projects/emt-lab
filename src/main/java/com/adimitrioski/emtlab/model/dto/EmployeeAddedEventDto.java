package com.adimitrioski.emtlab.model.dto;

import com.adimitrioski.emtlab.model.domain.Employee;

public class EmployeeAddedEventDto {
    public Employee employee;
    public String rawPassword;
}
