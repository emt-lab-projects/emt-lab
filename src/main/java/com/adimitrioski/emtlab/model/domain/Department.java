package com.adimitrioski.emtlab.model.domain;

import org.hibernate.search.annotations.*;

import javax.persistence.*;
import java.util.Set;

@Indexed
@Entity
@Table(name = "departments")
public class Department {


    @Id
    @GeneratedValue
    public Long id;

    @Field(index = org.hibernate.search.annotations.Index.YES, store = Store.NO, analyze = Analyze.YES)
    @Analyzer(definition = "emtAnalyser")
    @Column(name = "name", unique = true)
    public String name;

    @OneToMany(mappedBy = "department")
    public Set<Employee> employees;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(Set<Employee> employees) {
        this.employees = employees;
    }
}
