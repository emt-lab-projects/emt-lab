package com.adimitrioski.emtlab.model.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Set;

public class IntermediateUserDetails implements UserDetails {

    private static final long serialVersionUID = 1L;
    private IntermediateUser intermediateUser;
    private Set<GrantedAuthority> authorities;

    public IntermediateUserDetails(IntermediateUser intermediateUser) {
        this.intermediateUser = intermediateUser;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Set<GrantedAuthority> authorities) {
        this.authorities = authorities;
    }

    @Override
    public String getPassword() {
        return "";
    }

    @Override
    public String getUsername() {
        return intermediateUser.email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public IntermediateUser getIntermediateUser() {
        return intermediateUser;
    }

    public void setIntermediateUser(IntermediateUser intermediateUser) {
        this.intermediateUser = intermediateUser;
    }

}
