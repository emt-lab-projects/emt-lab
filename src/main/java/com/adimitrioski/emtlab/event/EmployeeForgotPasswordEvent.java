package com.adimitrioski.emtlab.event;

import com.adimitrioski.emtlab.model.domain.Employee;
import org.springframework.context.ApplicationEvent;

public class EmployeeForgotPasswordEvent extends ApplicationEvent {
    /**
     * Create a new ApplicationEvent.
     *
     * @param source the object on which the event initially occurred (never {@code null})
     */
    public EmployeeForgotPasswordEvent(Object source) {
        super(source);
    }

    public Employee getEmployee() {
        return (Employee) getSource();
    }
}
