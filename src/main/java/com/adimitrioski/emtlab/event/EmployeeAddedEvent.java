package com.adimitrioski.emtlab.event;

import com.adimitrioski.emtlab.model.dto.EmployeeAddedEventDto;
import org.springframework.context.ApplicationEvent;

public class EmployeeAddedEvent extends ApplicationEvent {
    /**
     * Create a new ApplicationEvent.
     *
     * @param source the object on which the event initially occurred (never {@code null})
     */
    public EmployeeAddedEvent(Object source) {
        super(source);
    }

    public EmployeeAddedEventDto getEmployeeAddedEventDto() {
        return (EmployeeAddedEventDto) getSource();
    }
}
