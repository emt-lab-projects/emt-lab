package com.adimitrioski.emtlab.initialization;

import com.adimitrioski.emtlab.model.domain.Department;
import com.adimitrioski.emtlab.model.domain.Employee;
import com.adimitrioski.emtlab.model.enumeration.Role;
import com.adimitrioski.emtlab.service.DepartmentService;
import com.adimitrioski.emtlab.service.EmployeeService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class DatabaseInit {

    private final EmployeeService employeeService;
    private final DepartmentService departmentService;
    private final PasswordEncoder passwordEncoder;

    public DatabaseInit(EmployeeService employeeService, DepartmentService departmentService, PasswordEncoder passwordEncoder) {
        this.employeeService = employeeService;
        this.departmentService = departmentService;
        this.passwordEncoder = passwordEncoder;
    }

    @PostConstruct
    public void init() {
        seedDepartments();
        seedEmployees();
    }

    private void seedDepartments() {
        if (departmentService.count() == 0) {

            Department departmentAdministration = new Department();
            Department departmentA = new Department();
            Department departmentB = new Department();

            departmentAdministration.name = "Administration";
            departmentA.name = "DepartmentA";
            departmentB.name = "DepartmentB";

            departmentService.save(departmentAdministration);
            departmentService.save(departmentA);
            departmentService.save(departmentB);
        }
    }

    private void seedEmployees() {
        if (employeeService.count() == 0) {

            Employee admin = new Employee();
            admin.firstName = "Admin";
            admin.lastName = "Admin";
            admin.email = "admin@admin.com";
            admin.password = passwordEncoder.encode("admin");
            String departmentName = "Administration";
            admin.department = departmentService.findByName(departmentName);
            admin.active = true;
            admin.role = Role.ADMIN;
            employeeService.save(admin);

            Employee managerA = new Employee();
            Employee managerB = new Employee();

            managerA.firstName = "ManagerA";
            managerB.firstName = "ManagerB";
            managerA.lastName = "ManagerA";
            managerB.lastName = "ManagerB";
            managerA.email = "managerA@managerA.com";
            managerB.email = "managerB@managerB.com";
            managerA.password = passwordEncoder.encode("managerA");
            managerB.password = passwordEncoder.encode("managerB");
            managerA.department = departmentService.findByName("DepartmentA");
            managerB.department = departmentService.findByName("DepartmentB");
            managerA.active = true;
            managerB.active = true;
            managerA.role = Role.MANAGER;
            managerB.role = Role.MANAGER;
            employeeService.save(managerA);
            employeeService.save(managerB);

            Employee employeeA1 = new Employee();
            Employee employeeA2 = new Employee();
            Employee employeeA3 = new Employee();

            employeeA1.firstName = "EmployeeA1";
            employeeA2.firstName = "EmployeeA2";
            employeeA3.firstName = "EmployeeA3";
            employeeA1.lastName = "EmployeeA1";
            employeeA2.lastName = "EmployeeA2";
            employeeA3.lastName = "EmployeeA3";
            employeeA1.email = "employeeA1@employeeA1.com";
            employeeA2.email = "employeeA2@employeeA2.com";
            employeeA3.email = "employeeA3@employeeA3.com";
            employeeA1.password = passwordEncoder.encode("employeeA1");
            employeeA2.password = passwordEncoder.encode("employeeA2");
            employeeA3.password = passwordEncoder.encode("employeeA3");

            Department departmentA = departmentService.findByName("DepartmentA");

            employeeA1.department = departmentA;
            employeeA2.department = departmentA;
            employeeA3.department = departmentA;
            employeeA1.active = true;
            employeeA2.active = true;
            employeeA3.active = true;
            employeeA1.role = Role.EMPLOYEE;
            employeeA2.role = Role.EMPLOYEE;
            employeeA3.role = Role.EMPLOYEE;
            employeeService.save(employeeA1);
            employeeService.save(employeeA2);
            employeeService.save(employeeA3);

            Employee employeeB1 = new Employee();
            Employee employeeB2 = new Employee();
            Employee employeeB3 = new Employee();

            employeeB1.firstName = "EmployeeB1";
            employeeB2.firstName = "EmployeeB2";
            employeeB3.firstName = "EmployeeB3";
            employeeB1.lastName = "EmployeeB1";
            employeeB2.lastName = "EmployeeB2";
            employeeB3.lastName = "EmployeeB3";
            employeeB1.email = "employeeB1@employeeB1.com";
            employeeB2.email = "employeeB2@employeeB2.com";
            employeeB3.email = "employeeB3@employeeB3.com";
            employeeB1.password = passwordEncoder.encode("employeeB1");
            employeeB2.password = passwordEncoder.encode("employeeB2");
            employeeB3.password = passwordEncoder.encode("employeeB3");

            Department departmentB = departmentService.findByName("DepartmentB");

            employeeB1.department = departmentB;
            employeeB2.department = departmentB;
            employeeB3.department = departmentB;
            employeeB1.active = true;
            employeeB2.active = true;
            employeeB3.active = true;
            employeeB1.role = Role.EMPLOYEE;
            employeeB2.role = Role.EMPLOYEE;
            employeeB3.role = Role.EMPLOYEE;
            employeeService.save(employeeB1);
            employeeService.save(employeeB2);
            employeeService.save(employeeB3);
        }
    }

}
