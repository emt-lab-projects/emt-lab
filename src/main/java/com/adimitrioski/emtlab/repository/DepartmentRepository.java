package com.adimitrioski.emtlab.repository;

import com.adimitrioski.emtlab.model.domain.Department;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface DepartmentRepository {

    Optional<Department> findById(Long id);

    Optional<Department> findByName(String name);

    List<Department> findAll();

    Page<Department> findAll(Pageable pageable);

    Page<Department> findAll(Example<Department> departmentExample, Pageable pageable);

    Department save(Department department);

    Department delete(Department department);

    Long count();

}
