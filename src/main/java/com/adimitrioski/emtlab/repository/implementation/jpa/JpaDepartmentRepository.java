package com.adimitrioski.emtlab.repository.implementation.jpa;

import com.adimitrioski.emtlab.model.domain.Department;
import com.adimitrioski.emtlab.repository.DepartmentRepository;
import org.springframework.data.repository.Repository;

public interface JpaDepartmentRepository extends DepartmentRepository, Repository<Department, Long> {
}
