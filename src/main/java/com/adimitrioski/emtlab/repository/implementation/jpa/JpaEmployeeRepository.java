package com.adimitrioski.emtlab.repository.implementation.jpa;

import com.adimitrioski.emtlab.model.domain.Employee;
import com.adimitrioski.emtlab.repository.EmployeeRepository;
import org.springframework.data.repository.Repository;

public interface JpaEmployeeRepository extends EmployeeRepository, Repository<Employee, Long> {
}
