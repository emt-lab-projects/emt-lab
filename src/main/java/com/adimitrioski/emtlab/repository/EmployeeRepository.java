package com.adimitrioski.emtlab.repository;

import com.adimitrioski.emtlab.model.domain.Employee;
import com.adimitrioski.emtlab.model.enumeration.Gender;
import com.adimitrioski.emtlab.model.enumeration.Role;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface EmployeeRepository {

    Optional<Employee> findById(Long id);

    Optional<Employee> findByEmail(String email);

    Optional<Employee> findByActivationCode(String activationCode);

    Page<Employee> findAll(Pageable pageable);

    Page<Employee> findAll(Example<Employee> employeeExample, Pageable pageable);

    List<Employee> findAll();

    List<Employee> findByDepartment(Long departmentId);

    List<Employee> findByRole(Role role);

    List<Employee> findByGender(Gender gender);

    List<Employee> findByActive(Boolean active);

    Employee save(Employee employee);

    Employee delete(Employee employee);

    Long count();

}
