package com.adimitrioski.emtlab.web.controller;


import com.adimitrioski.emtlab.web.exception.DepartmentDuplicateNameException;
import com.adimitrioski.emtlab.web.exception.EmployeeAlreadyActivatedException;
import com.adimitrioski.emtlab.web.exception.EmployeeDuplicateEmailException;
import com.adimitrioski.emtlab.web.exception.EmployeeWrongOldPasswordException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.io.IOException;


@ControllerAdvice
public class ExceptionHandlingController {

    @ExceptionHandler(DataRetrievalFailureException.class)
    public ResponseEntity<String> handleDataRetrievalFailureException(DataRetrievalFailureException e) {
        e.printStackTrace();
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    public ResponseEntity<String> handleDataIntegrityViolationException(DataIntegrityViolationException e) {
        e.printStackTrace();
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(DepartmentDuplicateNameException.class)
    public ResponseEntity<String> handleDepartmentDuplicateNameException(DepartmentDuplicateNameException e) {
        e.printStackTrace();
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(EmployeeDuplicateEmailException.class)
    public ResponseEntity<String> handleEmployeeDuplicateEmailException(EmployeeDuplicateEmailException e) {
        e.printStackTrace();
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(EmployeeAlreadyActivatedException.class)
    public ResponseEntity<String> handleEmployeeAlreadyActivatedException(EmployeeAlreadyActivatedException e) {
        e.printStackTrace();
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(UsernameNotFoundException.class)
    public ResponseEntity<String> handleUsernameNotFoundException(UsernameNotFoundException e) {
        e.printStackTrace();
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(IOException.class)
    public ResponseEntity<String> handleIOException(IOException e) {
        e.printStackTrace();
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(EmployeeWrongOldPasswordException.class)
    public ResponseEntity<String> handleEmployeeWrongOldPasswordException(EmployeeWrongOldPasswordException e) {
        e.printStackTrace();
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }

}
