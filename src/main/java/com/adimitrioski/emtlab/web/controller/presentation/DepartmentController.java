package com.adimitrioski.emtlab.web.controller.presentation;

import com.adimitrioski.emtlab.model.domain.Department;
import com.adimitrioski.emtlab.model.dto.DepartmentDto;
import com.adimitrioski.emtlab.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.util.List;

@Controller
public class DepartmentController {

    private final DepartmentService departmentService;

    @Autowired
    public DepartmentController(DepartmentService departmentService) {
        this.departmentService = departmentService;
    }

    @RequestMapping(value = "/departments/manage", method = RequestMethod.GET)
    public ModelAndView showDepartmentsManage() {
        ModelAndView modelAndView = new ModelAndView();
        List<Department> departments = departmentService.findAll();
        modelAndView.addObject("departments", departments);
        modelAndView.setViewName("departments-manage");
        return modelAndView;
    }

    @RequestMapping(value = "/departments/new", method = RequestMethod.GET)
    public ModelAndView showDepartmentNew() {
        ModelAndView modelAndView = new ModelAndView();
        DepartmentDto departmentDto = new DepartmentDto();
        modelAndView.addObject("department", departmentDto);
        modelAndView.setViewName("departments-new");
        return modelAndView;
    }

    @RequestMapping(value = "/departments/new", method = RequestMethod.POST)
    public String processDepartmentNew(@ModelAttribute("department") DepartmentDto employeeDto) throws IOException {
        departmentService.createNewDepartment(employeeDto);
        return "redirect:/departments/manage";
    }

    @RequestMapping(value = "/departments/edit/{id}", method = RequestMethod.GET)
    public ModelAndView showDepartmentEdit(@PathVariable Long id) {
        ModelAndView modelAndView = new ModelAndView();

        Department department = departmentService.findById(id);
        DepartmentDto departmentDto = new DepartmentDto();

        modelAndView.addObject("department", department);
        modelAndView.addObject("departmentDto", departmentDto);
        modelAndView.setViewName("departments-edit");

        return modelAndView;
    }

    @RequestMapping(value = "/departments/edit/{id}", method = RequestMethod.POST)
    public String processDepartmentEdit(@PathVariable Long id, @ModelAttribute("department") DepartmentDto departmentDto) throws IOException {
        departmentService.updateDepartment(id, departmentDto);
        return "redirect:/departments/manage";
    }

}
