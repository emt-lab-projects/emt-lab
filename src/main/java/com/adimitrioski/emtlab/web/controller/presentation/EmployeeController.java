package com.adimitrioski.emtlab.web.controller.presentation;

import com.adimitrioski.emtlab.model.domain.Department;
import com.adimitrioski.emtlab.model.domain.Employee;
import com.adimitrioski.emtlab.model.dto.EmployeeDto;
import com.adimitrioski.emtlab.model.enumeration.Role;
import com.adimitrioski.emtlab.model.security.EmployeeUserDetails;
import com.adimitrioski.emtlab.model.security.IntermediateUser;
import com.adimitrioski.emtlab.model.security.IntermediateUserDetails;
import com.adimitrioski.emtlab.service.DepartmentService;
import com.adimitrioski.emtlab.service.EmployeeSearchService;
import com.adimitrioski.emtlab.service.EmployeeService;
import com.adimitrioski.emtlab.web.exception.EmployeeWrongOldPasswordException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class EmployeeController {

    private static final String PAGE_ORDER_BY_CRITERIA = "firstName";

    private final EmployeeService employeeService;
    private final EmployeeSearchService employeeSearchService;
    private final DepartmentService departmentService;

    @Autowired
    public EmployeeController(EmployeeService employeeService, EmployeeSearchService employeeSearchService, DepartmentService departmentService) {
        this.employeeService = employeeService;
        this.employeeSearchService = employeeSearchService;
        this.departmentService = departmentService;
    }

    private static String buildPageableURIFromParams(String searchBy, String phrase, Integer page, Integer size) {
        return "/?searchBy=" + searchBy + "&phrase=" + phrase + "&page=" + page + "&size=" + size;
    }

    @RequestMapping(value = "/employees/register", method = RequestMethod.GET)
    public ModelAndView showEmployeeRegister() {

        ModelAndView modelAndView = new ModelAndView();
        EmployeeDto employeeDto = new EmployeeDto();
        modelAndView.addObject("employee", employeeDto);
        modelAndView.setViewName("employees-register");

        return modelAndView;
    }

    @RequestMapping(value = "/employees/register", method = RequestMethod.POST)
    public ModelAndView processEmployeeRegistration(@ModelAttribute("employee") EmployeeDto employeeDto) throws IOException {

        employeeService.registerNewEmployee(employeeDto);

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("successMessageMain", "Registration successful!");
        modelAndView.addObject("successMessageOther", "Check your email for activation code!");
        modelAndView.setViewName("common-success");
        return modelAndView;

    }

    @RequestMapping(value = "/employees/new", method = RequestMethod.GET)
    public ModelAndView showEmployeeNew() {

        ModelAndView modelAndView = new ModelAndView();
        EmployeeDto employeeDto = new EmployeeDto();
        List<Department> departments = departmentService.findAll();
        List<Role> roles = new ArrayList<>(Arrays.asList(Role.values()));

        modelAndView.addObject("employee", employeeDto);
        modelAndView.addObject("departments", departments);
        modelAndView.addObject("roles", roles);
        modelAndView.setViewName("employees-new");

        return modelAndView;
    }

    @RequestMapping(value = "/employees/activate", method = RequestMethod.GET)
    public String showEmployeeActivate() {
        return "employees-activate";
    }

    @RequestMapping(value = "/employees/new", method = RequestMethod.POST)
    public String processEmployeeNew(@ModelAttribute("employee") EmployeeDto employeeDto) throws IOException {
        employeeService.addNewEmployee(employeeDto);
        return "redirect:/employees/manage";
    }



    @RequestMapping(value = "/employees/request-password-reset", method = RequestMethod.GET)
    public String showEmployeeRequestPasswordReset() {
        return "employees-request-password-reset";
    }

    @RequestMapping(value = "/employees/activate", method = RequestMethod.POST)
    public ModelAndView processEmployeeActivation(@RequestParam String activationCode) {

        employeeService.activateEmployee(activationCode);

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("employees-login");
        modelAndView.addObject("hasInformation", true);
        modelAndView.addObject("information", "Activation successful, you may now login!");
        IntermediateUserDetails intermediateUserDetails = (IntermediateUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        IntermediateUser intermediateUser = intermediateUserDetails.getIntermediateUser();
        modelAndView.addObject("intermediateUser", intermediateUser);

        return modelAndView;
    }

    @RequestMapping(value = "/employees/change-password", method = RequestMethod.GET)
    public String showEmployeeChangePassword() {
        return "employees-change-password";
    }

    @RequestMapping(value = "/employees/change-password", method = RequestMethod.POST)
    public ModelAndView processEmployeeChangePassword(@RequestParam String oldPassword, String newPassword) {

        EmployeeUserDetails employeeUserDetails = (EmployeeUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Employee employee = employeeUserDetails.getEmployee();

        ModelAndView modelAndView = new ModelAndView();

        try {
            employeeService.changePassword(employee, oldPassword, newPassword);
        } catch (EmployeeWrongOldPasswordException e) {
            modelAndView.addObject("hasInformation", true);
            modelAndView.addObject("information", e.getMessage());
            modelAndView.setViewName("employees-change-password");
            return modelAndView;
        }

        return new ModelAndView(new RedirectView("/logout"));
    }

    @RequestMapping(value = "/employees/login", method = RequestMethod.GET)
    public ModelAndView showEmployeeLogin() {
        IntermediateUserDetails intermediateUserDetails = (IntermediateUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        IntermediateUser intermediateUser = intermediateUserDetails.getIntermediateUser();

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("intermediateUser", intermediateUser);
        modelAndView.setViewName("employees-login");

        return modelAndView;
    }

    @RequestMapping(value = "/employees/request-password-reset", method = RequestMethod.POST)
    public ModelAndView processEmployeeRequestPasswordReset(@RequestParam String email) {

        employeeService.requestResetPasswordEmployee(email);

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("common-success");
        modelAndView.addObject("successMessageMain", "Password reset request successful!");
        modelAndView.addObject("successMessageOther", "New password has been sent to your email!");

        return modelAndView;
    }

    @RequestMapping(value = "/employees/edit/my-profile", method = RequestMethod.GET)
    public ModelAndView showEmployeeEditMyProfile() {

        EmployeeUserDetails employeeUserDetails = (EmployeeUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Employee employee = employeeUserDetails.getEmployee();
        EmployeeDto employeeDto = new EmployeeDto();

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("employee", employee);
        modelAndView.addObject("employeeDto", employeeDto);

        String employeeAvatarEncoded = "";
        if (employee.getAvatar() != null) {
            employeeAvatarEncoded = Base64.getEncoder().encodeToString(employee.getAvatar());
        }
        modelAndView.addObject("employeeAvatarEncoded", employeeAvatarEncoded);

        modelAndView.setViewName("employees-my-profile");

        return modelAndView;
    }


    @RequestMapping(value = "/employees/edit/my-profile", method = RequestMethod.POST)
    public String processEmployeeEditMyProfile(@ModelAttribute("employee") EmployeeDto employeeDto) throws IOException {
        EmployeeUserDetails employeeUserDetails = (EmployeeUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Employee employee = employeeUserDetails.getEmployee();
        employeeService.updateEmployee(employee.id, employeeDto);
        return "redirect:/";
    }

    @RequestMapping(value = "/employees/edit/{id}", method = RequestMethod.GET)
    public ModelAndView showEmployeeEdit(@PathVariable Long id) {

        EmployeeUserDetails employeeUserDetails = (EmployeeUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Employee loggedInEmployee = employeeUserDetails.getEmployee();

        if (loggedInEmployee.id.equals(id)) {
            return showEmployeeEditMyProfile();
        }

        Employee employee = employeeService.findById(id);
        EmployeeDto employeeDto = new EmployeeDto();
        List<Department> departments = departmentService.findAll();
        List<Role> roles = new ArrayList<>(Arrays.asList(Role.values()));

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("employee", employee);
        modelAndView.addObject("employeeDto", employeeDto);
        modelAndView.addObject("departments", departments);
        modelAndView.addObject("roles", roles);

        String employeeAvatarEncoded = "";
        if (employee.getAvatar() != null) {
            employeeAvatarEncoded = Base64.getEncoder().encodeToString(employee.getAvatar());
        }

        modelAndView.setViewName("employees-edit");

        return modelAndView;
    }

    @RequestMapping(value = "/employees/edit/{id}", method = RequestMethod.POST)
    public String processEmployeeEdit(@PathVariable Long id, @ModelAttribute("employee") EmployeeDto employeeDto) throws IOException {
        employeeService.updateEmployee(id, employeeDto);
        return "redirect:/employees/manage";
    }

    @RequestMapping(value = "/employees/delete/{id}", method = RequestMethod.GET)
    public String deleteEmployee(@PathVariable Long id) {
        employeeService.delete(id);
        return "redirect:/employees/manage";
    }

    private Pageable getPageable(Integer page, Integer size) {
        return PageRequest.of(page, size, new Sort(Sort.Direction.ASC, PAGE_ORDER_BY_CRITERIA));
    }

    @RequestMapping(value = "/employees/manage", method = RequestMethod.GET)
    public ModelAndView showEmployeesManage(@RequestParam(required = false, defaultValue = "") String searchBy, @RequestParam(required = false, defaultValue = "") String phrase, @RequestParam(required = false, defaultValue = "0") Integer page, @RequestParam(required = false, defaultValue = "1") Integer size) {

        Pageable pageable = getPageable(page, 20);
        Page<Employee> employeePage = null;
        if (searchBy == null || searchBy.isEmpty()) {
            employeePage = employeeService.findAll(pageable);
        } else {
            if (searchBy.compareTo("email") == 0) {
                employeePage = employeeSearchService.findByEmail(phrase, pageable);
            } else if (searchBy.compareTo("firstName") == 0) {
                employeePage = employeeSearchService.findByFirstName(phrase, pageable);
            } else if (searchBy.compareTo("lastName") == 0) {
                employeePage = employeeSearchService.findByLastName(phrase, pageable);
            }
        }

        List<Employee> employees = employeePage.getContent();

        EmployeeUserDetails employeeUserDetails = (EmployeeUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Employee loggedInEmployee = employeeUserDetails.getEmployee();
        if (loggedInEmployee.role.equals(Role.MANAGER)) {
            employees = employees.stream().filter(e -> e.department.id.equals(loggedInEmployee.department.id)).collect(Collectors.toList());
        }

        Department selectedEmployeeDepartment = new Department();
        Employee selectedEmployee = new Employee();
        selectedEmployee.department = selectedEmployeeDepartment;

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("employees", employees);
        modelAndView.addObject("selectedEmployee", selectedEmployee);
        modelAndView.addObject("phrase", phrase);
        modelAndView.addObject("prevPageRequest", buildPageableURIFromParams(searchBy, phrase, employeePage.isFirst() ? 0 : employeePage.getNumber() - 1, size));
        modelAndView.addObject("nextPageRequest", buildPageableURIFromParams(searchBy, phrase, employeePage.isLast() ? employeePage.getNumber() : employeePage.getNumber() + 1, size));
        modelAndView.addObject("prevPage", employeePage.isFirst() ? 0 : employeePage.getNumber() - 1);
        modelAndView.addObject("nextPage", employeePage.isLast() ? employeePage.getNumber() : employeePage.getNumber() + 1);
        modelAndView.addObject("hasNext", employeePage.hasNext());
        modelAndView.addObject("hasPrev", employeePage.hasPrevious());
        modelAndView.addObject("pageNumber", employeePage.getNumber());
        modelAndView.addObject("totalPagesNumber", employeePage.getTotalPages());


        modelAndView.setViewName("employees-manage");

        return modelAndView;
    }

    @RequestMapping(value = "/employees/chat", method = RequestMethod.GET)
    public ModelAndView showEmployeesChat() {
        EmployeeUserDetails employeeUserDetails = (EmployeeUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Employee employee = employeeUserDetails.getEmployee();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("employee", employee);
        modelAndView.setViewName("employees-chat");
        return modelAndView;
    }

}
