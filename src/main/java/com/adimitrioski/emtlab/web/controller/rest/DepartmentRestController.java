package com.adimitrioski.emtlab.web.controller.rest;

import com.adimitrioski.emtlab.model.domain.Department;
import com.adimitrioski.emtlab.model.dto.DepartmentDto;
import com.adimitrioski.emtlab.service.DepartmentService;
import com.adimitrioski.emtlab.service.SearchService;
import com.adimitrioski.emtlab.web.exception.DepartmentDuplicateNameException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api", produces = MediaType.APPLICATION_JSON_VALUE)
public class DepartmentRestController {

    private static final String PAGE_ORDER_BY_CRITERIA = "name";

    private final DepartmentService departmentService;
    private final SearchService searchService;

    @Autowired
    public DepartmentRestController(DepartmentService departmentService, SearchService searchService) {
        this.departmentService = departmentService;
        this.searchService = searchService;
    }

    @RequestMapping(value = "departments", method = RequestMethod.POST)
    public Department createNewDepartment(@RequestBody DepartmentDto departmentDto) throws DepartmentDuplicateNameException, DataIntegrityViolationException {
        return departmentService.createNewDepartment(departmentDto);
    }

    @RequestMapping(value = "departments/all", method = RequestMethod.GET)
    public List<Department> findAll() {
        return departmentService.findAll();
    }

    @RequestMapping(value = "departments", method = RequestMethod.GET)
    public Page<Department> findAll(@RequestParam(required = false, defaultValue = "0") Integer page, @RequestParam(required = false, defaultValue = "1") Integer size) {
        Pageable pageable = getPageable(page, size);
        return departmentService.findAll(pageable);
    }

    @RequestMapping(value = "departments/{id}", method = RequestMethod.PATCH)
    public Department updateDepartment(@PathVariable Long id, @RequestBody DepartmentDto departmentDto) throws DepartmentDuplicateNameException, DataIntegrityViolationException {
        return departmentService.updateDepartment(id, departmentDto);
    }

    @RequestMapping(value = "departments/{id}", method = RequestMethod.GET)
    public Department findById(@PathVariable Long id) throws DataRetrievalFailureException {
        return departmentService.findById(id);
    }

    @RequestMapping(value = "departments/count", method = RequestMethod.GET)
    public Long count() {
        return departmentService.count();
    }

    private Pageable getPageable(Integer page, Integer size) {
        return PageRequest.of(page, size, new Sort(Sort.Direction.ASC, PAGE_ORDER_BY_CRITERIA));
    }

    @RequestMapping(value = "departments/search", method = RequestMethod.GET)
    public List<Department> search(@RequestParam String phrase) {
        return searchService.searchDepartments(phrase);
    }
}
