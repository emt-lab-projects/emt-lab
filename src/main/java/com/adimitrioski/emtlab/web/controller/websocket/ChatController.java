package com.adimitrioski.emtlab.web.controller.websocket;

import com.adimitrioski.emtlab.configuration.websocket.WebSocketConfig;
import com.adimitrioski.emtlab.model.websocket.ChatMessage;
import com.adimitrioski.emtlab.service.WebSocketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.time.LocalDateTime;


@Controller
public class ChatController {

    private final WebSocketService webSocketService;

    @Autowired
    public ChatController(WebSocketService webSocketService) {
        this.webSocketService = webSocketService;
    }

    @RequestMapping(value = "/send", method = RequestMethod.POST,
            produces = "application/json")
    @ResponseBody
    public void send(@RequestParam String user, @RequestParam String message) {
        ChatMessage chatMessage = new ChatMessage(user, message, LocalDateTime.now());
        webSocketService.send(WebSocketConfig.DEFAULT_TOPIC, chatMessage);
    }


}
