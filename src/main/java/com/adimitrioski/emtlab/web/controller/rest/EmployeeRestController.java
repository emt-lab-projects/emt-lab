package com.adimitrioski.emtlab.web.controller.rest;


import com.adimitrioski.emtlab.model.domain.Employee;
import com.adimitrioski.emtlab.model.dto.EmployeeDto;
import com.adimitrioski.emtlab.service.EmployeeService;
import com.adimitrioski.emtlab.service.SearchService;
import com.adimitrioski.emtlab.web.exception.EmployeeAlreadyActivatedException;
import com.adimitrioski.emtlab.web.exception.EmployeeDuplicateEmailException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping(value = "/api", produces = MediaType.APPLICATION_JSON_VALUE)
public class EmployeeRestController {

    private static final String PAGE_ORDER_BY_CRITERIA = "firstName";

    private final EmployeeService employeeService;
    private final SearchService searchService;

    @Autowired
    public EmployeeRestController(EmployeeService employeeService, SearchService searchService) {
        this.employeeService = employeeService;
        this.searchService = searchService;
    }

    @RequestMapping(value = "employees/register", method = RequestMethod.POST)
    public Employee registerNewEmployee(@RequestBody EmployeeDto employeeDto) throws EmployeeDuplicateEmailException, DataRetrievalFailureException, IOException {
        return employeeService.registerNewEmployee(employeeDto);
    }

    @RequestMapping(value = "employees/new", method = RequestMethod.POST)
    public Employee addNewEmployee(@RequestBody EmployeeDto employeeDto) throws EmployeeDuplicateEmailException, DataRetrievalFailureException, IOException {
        return employeeService.addNewEmployee(employeeDto);
    }

    @RequestMapping(value = "employees/all", method = RequestMethod.GET)
    public List<Employee> findAll() {
        return employeeService.findAll();
    }

    @RequestMapping(value = "employees", method = RequestMethod.GET)
    public Page<Employee> findAll(@RequestParam(required = false, defaultValue = "0") Integer page, @RequestParam(required = false, defaultValue = "1") Integer size) {
        Pageable pageable = getPageable(page, size);
        return employeeService.findAll(pageable);
    }

    @RequestMapping(value = "employees/{id}", method = RequestMethod.GET)
    public Employee findById(@PathVariable Long id) throws DataRetrievalFailureException {
        return employeeService.findById(id);
    }

    @RequestMapping(value = "employees/{id}", method = RequestMethod.PATCH)
    public Employee updateEmployee(@PathVariable Long id, @RequestBody EmployeeDto employeeDto) throws DataRetrievalFailureException, DataIntegrityViolationException, IOException {
        return employeeService.updateEmployee(id, employeeDto);
    }

    @RequestMapping(value = "employees/activate/{activationCode}", method = RequestMethod.POST)
    public Employee activateEmployee(@PathVariable String activationCode) throws EmployeeAlreadyActivatedException, DataRetrievalFailureException, DataIntegrityViolationException {
        return employeeService.activateEmployee(activationCode);
    }

    @RequestMapping(value = "employees/request-reset-password", method = RequestMethod.POST)
    public Employee requestResetPasswordEmployee(@RequestParam String email) throws DataRetrievalFailureException, DataIntegrityViolationException {
        return employeeService.requestResetPasswordEmployee(email);
    }

    @RequestMapping(value = "employees/change-password", method = RequestMethod.POST)
    public Employee changePasswordEmployee(@RequestParam String oldPassword, String newPassword) throws DataIntegrityViolationException {
        return employeeService.changePassword(null, oldPassword, newPassword);
    }

    @RequestMapping(value = "employees/{id}", method = RequestMethod.DELETE)
    public Employee delete(@PathVariable Long id) throws DataRetrievalFailureException {
        return employeeService.delete(id);
    }

    @RequestMapping(value = "employees/department/{id}", method = RequestMethod.GET)
    public List<Employee> findByDepartment(@PathVariable Long id) throws DataRetrievalFailureException {
        return employeeService.findByDepartment(id);
    }

    @RequestMapping(value = "employees/count", method = RequestMethod.GET)
    public Long count() {
        return employeeService.count();
    }

    private Pageable getPageable(Integer page, Integer size) {
        return PageRequest.of(page, size, new Sort(Sort.Direction.ASC, PAGE_ORDER_BY_CRITERIA));
    }

    @RequestMapping(value = "employees/search", method = RequestMethod.GET)
    public List<Employee> search(@RequestParam String phrase) {
        return searchService.searchEmployees(phrase);
    }


}
