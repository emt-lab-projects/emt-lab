package com.adimitrioski.emtlab.web.exception;

public class EmployeeDuplicateEmailException extends RuntimeException {

    public EmployeeDuplicateEmailException(String message) {
        super(message);
    }

}
