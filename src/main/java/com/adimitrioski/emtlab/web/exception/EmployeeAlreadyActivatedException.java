package com.adimitrioski.emtlab.web.exception;

public class EmployeeAlreadyActivatedException extends RuntimeException {

    public EmployeeAlreadyActivatedException(String message) {
        super(message);
    }

}
