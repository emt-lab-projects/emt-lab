package com.adimitrioski.emtlab.web.exception;

public class DepartmentDuplicateNameException extends RuntimeException {

    public DepartmentDuplicateNameException(String message) {
        super(message);
    }

}
