package com.adimitrioski.emtlab.web.exception;

public class EmployeeWrongOldPasswordException extends RuntimeException {

    public EmployeeWrongOldPasswordException(String message) {
        super(message);
    }

}
