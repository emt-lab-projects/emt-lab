package com.adimitrioski.emtlab.schedule;

import com.adimitrioski.emtlab.model.domain.Employee;
import com.adimitrioski.emtlab.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.LocalTime;
import java.util.List;

@Component
public class DeleteNotActivatedEmployees {

    private final EmployeeRepository employeeRepository;

    @Autowired
    public DeleteNotActivatedEmployees(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @Scheduled(cron = "00 55 23 * * *")
    public void deleteNotActivatedEmployees() {
        List<Employee> notActivatedEmployees = employeeRepository.findByActive(false);
        LocalTime currentTime = LocalTime.now();
        for (Employee e : notActivatedEmployees) {
            long hoursPassed = Duration.between(currentTime, e.registrationDate).toHours();
            if (hoursPassed >= 24) {
                employeeRepository.delete(e);
            }
        }
    }
}
